#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<gl/gl.h>
#include<gl/glu.h>
#include<math.h>
#define GL_PI 180

void draw()
{
  // Call only once for all remaining points
glBegin(GL_LINES);

float x,y,z,angle;

// All lines lie in the xy plane.
z = 0.0f;
for(angle = 0.0f; angle <= GL_PI; angle ++)
  {
  glColor3f(0.0,0.0,1.0);
  // Top half of the circle
  x = 1.0f*sin(angle);
  y = 1.0f*cos(angle);
  glVertex3f(x, y, z);    // First endpoint of line

  glColor3f(0.0,1.0,0.0);
  // Bottom half of the circle
  x = 1.0f*sin(angle + GL_PI);
  y = 1.0f*cos(angle + GL_PI);
  glVertex3f(x, y, z);    // Second endpoint of line
  }

// Done drawing points
glEnd();
}
void init()
{
    glClearColor(0.0,0.0,0.0,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,640.0/480.0,1.0,100.0);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);

}

    void display()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0,0.0,-3.0);
    //glRotatef(angle,1.0,1.0,1.0);
    glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);
    draw();

}

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);
    printf("OpenGL is running..");
    SDL_WM_SetCaption("KILLING FLAME",NULL);  //SET WINDOW CAPTION

    SDL_SetVideoMode(600,400,32,SDL_OPENGL);  //SET WINDOW SIZE
    //Main game loop

    //handles the main loop
     int isRunning=1;

    //for handling with event
    SDL_Event event;
	init();
    while(isRunning)
    {
        //input or enevts
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
            {
                isRunning = false;
            }
            if(event.type==SDL_KEYDOWN && event.key.keysym.sym==SDLK_ESCAPE)
            {
                isRunning = false;
            }

        }

        display();
        SDL_GL_SwapBuffers();

    }

    SDL_Quit();
    return 0;

}
